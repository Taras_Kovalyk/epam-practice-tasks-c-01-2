using System;

struct Point 
{
	public double x;
	public double y;
	
	public Point(double x1, double y1)
	{
		x = x1;
		y = y1;
	}
}

class Rectangle 
{
	private Point[] _rectangle;
	
	Rectangle(Point a, Point b, Point c, Point d)
	{
		if(a.x == b.x && b.y == c.y)
		{
			_rectangle = new Point[4];
            _rectangle[0] = a;
            _rectangle[1] = b;
            _rectangle[2] = c;
            _rectangle[3] = d;
		}
		else if(a.y == b.y && b.x == c.x)
		{
			_rectangle = new Point[4];
            _rectangle[0] = a;
            _rectangle[1] = b;
            _rectangle[2] = c;
            _rectangle[3] = d;
		}
		else
		{
			Console.WriteLine("Error. Wrong information. Is not rectangle or not paralel to axes");
		}
	}
	
	public void MoveRectangle(int x, int y)
	{
		for(int i = 0; i < _rectangle.Length; i++)
		{
			_rectangle[i].x += x;
			_rectangle[i].y += y;
		}
	}
	
	public void ScaleRectangle(int x, int y)
	{
		for(int i = 0; i < _rectangle.Length; i++)
		{
			_rectangle[i].x *= x;
			_rectangle[i].y *= y;
		}
	}
	
	public static Rectangle BuildRectangle (Rectangle r1, Rectangle r2)
	{
		double minX = r1._rectangle[0].x;
		double maxX = r1._rectangle[0].x;
		double minY = r1._rectangle[0].y;
		double maxY = r1._rectangle[0].y;
		
		for(int i = 0; i < r1._rectangle.Length; i++)
		{
			if(minX > r1._rectangle[i].x)
			{ 
				minX = r1._rectangle[i].x;
			}
			
			if(minY > r1._rectangle[i].y)
			{
				minY = r1._rectangle[i].y;
			}
			
			if(maxX < r1._rectangle[i].x)
			{
				maxX = r1._rectangle[i].x;
			}
			
			if(maxY < r1._rectangle[i].y)
			{
				maxY = r1._rectangle[i].y;
			}
		}
		
		for(int i = 0; i < r2._rectangle.Length; i++)
		{
			if(minX > r2._rectangle[i].x)
			{
				minX = r2._rectangle[i].x;
			}
			
			if(minY > r2._rectangle[i].y)
			{
				minY = r2._rectangle[i].y;
			}
			
			if(maxX < r2._rectangle[i].x)
			{
				maxX = r2._rectangle[i].x;
			}
			
			if(maxY < r2._rectangle[i].y)
			{
				maxY = r2._rectangle[i].y;
			}
		}
		
		return new Rectangle(new Point(minX, minY), new Point(minX, maxY), new Point(maxX, maxY), new Point(maxX, minY));
	}
	
	public static Rectangle IntersectRectangle (Rectangle r1, Rectangle r2)
	{
		if (r1._rectangle[3].y > r2._rectangle[1].y || r1._rectangle[1].y < r2._rectangle[3].y || r1._rectangle[1].x > r2._rectangle[3].x || 
		    r1._rectangle[3].x < r2._rectangle[1].x)
		{
			Console.WriteLine("Intersection not found");
			return null;
		}
		else
		{
			return new Rectangle(new Point(Math.Max(r1._rectangle[1].x, r2._rectangle[1].x), Math.Max(r1._rectangle[3].y, r2._rectangle[3].y)), 
			new Point(Math.Max(r1._rectangle[1].x, r2._rectangle[1].x), Math.Min(r1._rectangle[1].y, r2._rectangle[1].y)), 
			new Point(Math.Min(r1._rectangle[3].x, r2._rectangle[3].x), Math.Min(r1._rectangle[1].y, r2._rectangle[1].y)), 
			new Point(Math.Min(r1._rectangle[3].x, r2._rectangle[3].x), Math.Max(r1._rectangle[3].y, r2._rectangle[3].y)));
		}
	}
	
	static void Main()
	{
		var rect1 = new Rectangle(new Point(1, 1), new Point(1, 3), new Point(3, 3), new Point(3, 1));
		var rect2 = new Rectangle(new Point(4, 1), new Point(4, 3), new Point(9, 3), new Point(9, 1));
		var rect3 = new Rectangle(new Point(5, 2), new Point(5, 6), new Point(8, 6), new Point(8, 2));
		
		Console.WriteLine("Rectangle 1\n");
		for (int i = 0; i < rect1._rectangle.Length; i++)
		{
			Console.WriteLine("{0},{1}", rect1._rectangle[i].x, rect1._rectangle[i].y);
		}
		
		Console.WriteLine("\nRectangle 2\n");
		for (int i = 0; i < rect2._rectangle.Length; i++)
		{
			Console.WriteLine("{0},{1}", rect2._rectangle[i].x, rect2._rectangle[i].y);
		}
		
		Console.WriteLine("\nRectangle 3\n");
		for (int i = 0; i < rect3._rectangle.Length; i++)
		{
			Console.WriteLine("{0},{1}", rect3._rectangle[i].x, rect3._rectangle[i].y);
		}
		
		var buildTest = BuildRectangle(rect1, rect2);
		var interTest = IntersectRectangle(rect2, rect3);
				
		Console.WriteLine("\nBuild test result for rectangle 1 and rectangle 2\n");
		for (int i = 0; i < buildTest._rectangle.Length; i++)
		{
			Console.WriteLine("{0},{1}", buildTest._rectangle[i].x, buildTest._rectangle[i].y);
		}
		
		Console.WriteLine("\nIntersection test result for rectangle 2 and rectangle 3\n");
		for (int i = 0; i < interTest._rectangle.Length; i++)
		{
			Console.WriteLine("{0},{1}", interTest._rectangle[i].x, interTest._rectangle[i].y);
		}
		
		rect1.MoveRectangle(2, 2);
		rect2.ScaleRectangle(3, 2);
		
		Console.WriteLine("\nMove result (Move 2 on x axis and 2 on y axis) for regtangle 1\n");
		for (int i = 0; i < rect1._rectangle.Length; i++)
		{
			Console.WriteLine("{0},{1}", rect1._rectangle[i].x, rect1._rectangle[i].y);
		}
		
		Console.WriteLine("\nScale result (Scale 3 on x axis and 2 on y axis) for rectangle 2\n");
		for (int i = 0; i < rect2._rectangle.Length; i++)
		{
			Console.WriteLine("{0},{1}", rect2._rectangle[i].x, rect2._rectangle[i].y);
		}		
		
		Console.ReadLine();
	}
}